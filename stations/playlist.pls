[playlist]
NumberOfEntries=163
Title1=AEROESTEREO  94.3  FM
File1=http://stream.zeno.fm/8u6kahy9b9quv.pls

Title2=AEROESTEREO 94.3 FM
File2=http://stream.zeno.fm/8u6kahy9b9quv.m3u

Title3=Andina Radio 980 AM
File3=http://167.114.118.120:7058/;

Title4=Asia La Radio
File4=http://167.114.118.120:7804/;

Title5=Bienestar
File5=http://17963.live.streamtheworld.com/BIENESTAR.mp3?csegid=1010&dist=1010&tdsdk=js-%40td&pname=TDSdk&pversion=%40td&banners=none&sbmid=46ea3be7-3eeb-4b49-d2ce-8568fcb10be7

Title6=Bongara
File6=http://node-21.zeno.fm/q568uytu6mruv?1597624354113=&rj-ttl=5&rj-tok=AAABc_pkBp8A3a_HqKqwgHzpBw

Title7=Cadis
File7=http://node-11.zeno.fm/a440accyxqzuv?rj-ttl=5&rj-tok=AAABdAmfufkAJ1zwrPdEzR7t6Q

Title8=Candela-Bagua
File8=http://oyotunstream.com:9320/;

Title9=Central TV Perú
File9=http://cdn2.ujjina.com:1935/iptvcentraltv/livecentraltvtv/playlist.m3u8

Title10=Chalaca
File10=https://conectperu.com:7041/stream?icy=http

Title11=Clarín FM-Chachapoyas
File11=http://radio.ancashserver.com:9788/;

Title12=Corazón
File12=https://17833.live.streamtheworld.com/RADIO_CORAZON.mp3

Title13=Cumbia-Chachapoyas
File13=http://167.114.118.120:7698/;

Title14=Doble Nueve - Classic
File14=http://149.56.241.147:8008/stream?icy=http

Title15=Doble Nueve - Heritage
File15=http://149.56.241.147:8006/stream?icy=http

Title16=Doble Nueve - Live
File16=http://149.56.241.147:8002/stream?icy=http

Title17=Doble Nueve - Millenial
File17=http://149.56.241.147:8004/stream

Title18=ElectroPop Music
File18=http://listen.radionomy.com/electropop-music.m3u

Title19=Enamorados
File19=https://node-20.zeno.fm/b594gnc1fnruv.aac?rj-ttl=5&rj-tok=AAABdAo02tgAZi1na9IQnK30tw

Title20=Esfera-Chachapoyas
File20=http://oyotunstream.com:7360/;stream

Title21=Estación 90s Radio
File21=http://radio.ancashserver.com:9552/;

Title22=Estrella - Barranca
File22=https://sonic-us.streaming-chile.com:7039/;

Title23=Exclusiva-Luya
File23=https://sp.oyotunstream.com/8042/;

Title24=Felicidad
File24=https://18253.live.streamtheworld.com/RADIO_FELICIDAD.mp3

Title25=Feliz
File25=http://rafel.radioca.st:8135/;stream.mp3

Title26=FIDELIDAD 99.9 FM
File26=http://stream.zeno.fm/dg4u1cfm08quv.m3u

Title27=Filarmonía (OCR-4V, 102.7 MHz FM, Lima) (IRTP)
File27=http://m.mediastream.pe/av_filarmonia/livestream/playlist.m3u8

Title28=Filarmonía (OCR-4V, 102.7 MHz FM, Lima) (IRTP)
File28=http://c22.radioboss.fm:8100/stream

Title29=Filarmónica
File29=http://c22.radioboss.fm:8100/stream

Title30=Foro Radio 88.7 FM
File30=http://212.83.146.67:6312/;

Title31=Free FM Peru
File31=http://pollux.shoutca.st:8968/

Title32=Hits Star-Bagua
File32=http://oyotunstream.com:7278/;

Title33=Inca Sat
File33=http://17853.live.streamtheworld.com/CRP_INC.mp3?dist=20004

Title34=Indie Rock Radio
File34=http://listen.radionomy.com/indie-rock-radio

Title35=JR Radio
File35=http://99.198.110.162:8038/

Title36=K Pop Way Radio - Lima
File36=http://streamer.radio.co/s06b196587/listen

Title37=Karibeña
File37=http://167.114.118.120:7442/;

Title38=Karibeña
File38=http://167.114.118.120:7442/;

Title39=Kpopway Radio
File39=https://streamer.radio.co/s06b196587/listen.m3u

Title40=La Best Mix-Moyobamba
File40=http://node-23.zeno.fm/v1qe5y1vetzuv?rj-ttl=5&rj-tok=AAABc_pomJMAsLliLiG2yIvWiQ

Title41=La Inolvidable
File41=http://16613.live.streamtheworld.com/CRP_LI.mp3?dist=20004

Title42=La Kalle
File42=http://167.114.118.120:7448/;stream.mp3

Title43=La Mega
File43=https://16693.live.streamtheworld.com/RADIO_LAMEGA.mp3?DIST=RADIOWeb&TGT=RADIOWeb&maxServers=2&ua=RadioTime&ttag=RadioTime

Title44=La Rock N Pop
File44=http://listen.radionomy.com/retrorock-pop-latino.m3u

Title45=La Zona
File45=https://17833.live.streamtheworld.com/RADIO_LAZONA.mp3

Title46=Latidos 94.7 FM
File46=http://ip.peruhits.com:9944/;stream.mp3

Title47=Luz y sonido
File47=https://radioserver11.profesionalhosting.com/proxy/mekqyaic?mp=/stream

Title48=Mágica
File48=http://18553.live.streamtheworld.com/MAG.mp3?dist=20004

Title49=Marañon-Jaen
File49=http://167.114.118.120:7112/;

Title50=Maxi Mix
File50=http://node-26.zeno.fm/znv7mhnuaxquv?rj-ttl=5&rj-tok=AAABdAo3l1oAwK3bQVH0hQBHqQ

Title51=Moda
File51=http://18303.live.streamtheworld.com/CRP_MOD.mp3?dist=20004

Title52=Nacional
File52=http://17523.live.streamtheworld.com/RADIO_CAPITAL.mp3

Title53=Oasis
File53=http://19013.live.streamtheworld.com/CRP_OAS.mp3?dist=20004

Title54=Onda Azul
File54=http://167.114.118.120:7160/;

Title55=Oxígeno
File55=https://18493.live.streamtheworld.com/RADIO_OXIGENO.mp3

Title56=PERFECCION FM
File56=http://stream.zeno.fm/n350ttc3u8quv.m3u

Title57=Perú Folk Radio
File57=http://gnstreammedia.net:8020/;stream.aac

Title58=Planeta
File58=http://19013.live.streamtheworld.com/CRP_PLA.mp3?dist=20004

Title59=RADIO  KAPRICHOS  FM
File59=http://stream.zeno.fm/8bagnnetftzuv.pls

Title60=RADIO  PLATINUM  102.9  FM
File60=http://stream.zeno.fm/59sn98mhanruv.pls

Title61=Radio A1 88.7 - Cañete
File61=http://radio.ancashserver.com:9570/;

Title62=Radio Americana - Ilo
File62=http://167.114.118.120:7694/;

Title63=Radio Bahía 103.1 FM - Pisco
File63=http://167.114.118.119:7494/;

Title64=Radio Bienestar 1360 Lima
File64=http://provisioning.streamtheworld.com/pls/BIENESTARAAC.pls

Title65=Radio Canto Grande (OAR-4H, 97.7 MHz, Lima [eastern])
File65=http://65.60.2.26:8050/;

Title66=Radio Capital (OBR-4N, 96.7 MHz, Lima)
File66=http://playerservices.streamtheworld.com/pls/RADIO_CAPITAL.pls

Title67=Radio Churin
File67=http://167.114.118.119:7540/

Title68=Radio Comas AM (OAX-4S, 1300 kHz AM, Lima)
File68=http://65.60.2.26:8046/;

Title69=Radio Comas FM (OCW-4L, 101.7 MHz FM, Lima)
File69=http://65.60.2.26:8044/;

Title70=Radio Conoden
File70=http://stream.zeno.fm/wxpchhfw44zuv

Title71=Radio Corazón (OCR-4M, 94.3 MHz FM, Lima)
File71=http://playerservices.streamtheworld.com/pls/RADIO_CORAZON.pls

Title72=Radio Ctistiana La Senda Antigua
File72=http://stream.zeno.fm/k71xz7yh64zuv

Title73=Radio Cumbia (OAQ-9H, 107.1 MHz FM, Chachapoyas, Amazonas)
File73=http://167.114.118.120:7698/;

Title74=Radio Domino
File74=http://51.15.152.81:9142/stream

Title75=Radio Exitosa (OCW-4Z, 95.5 MHz, Lima)
File75=http://167.114.118.120:7444/;

Title76=Radio Exitosa 95.5 FM
File76=http://167.114.118.120:7444/;

Title77=Radio Felicidad FM (OCZ-4M, 88.9 MHz FM, Lima)
File77=http://playerservices.streamtheworld.com/pls/RADIO_FELICIDAD.pls

Title78=RADIO FESTIVAL FM
File78=http://stream.zeno.fm/rrymns2d7gruv.m3u

Title79=RADIO FIDELIDAD 99.9 FM
File79=http://stream.zeno.fm/dg4u1cfm08quv.m3u

Title80=RADIO FIDELIDAD 99.9 FM
File80=http://stream.zeno.fm/dg4u1cfm08quv.m3u

Title81=Radio frecuencia 100 - Trujillo
File81=http://oyotunstream.com:7066/;

Title82=Radio G la Estación - Lima
File82=http://195.154.167.62:7056/;

Title83=Radio Gozo
File83=http://radio.gisslive.com:9006/stream

Title84=Radio Inca (OBX-4E, 540 kHZ AM, Lima)
File84=http://playerservices.streamtheworld.com/pls/CRP_INCAAC.pls

Title85=Radio Izarra Digital 91.5 FM
File85=http://167.114.118.119:7440/;stream

Title86=Radio JR 88.7 FM - Arequipa
File86=http://99.198.110.162:8038/;

Title87=Radio La Hechicera
File87=http://67.212.179.138:9028/;

Title88=Radio La Inolvidable (OBT-4C, 93.7 MHz FM, Lima)
File88=http://provisioning.streamtheworld.com/pls/CRP_LI.pls

Title89=Radio La Kalle (OCR-4N, 96.1 MHz FM, Lima)
File89=http://167.114.118.120:7448/;

Title90=Radio La Karibeña
File90=https://onlineradiobox.com/json/pe/sabormix/play?platform=web

Title91=Radio La Nube
File91=https://streaming.gometri.com/stream/8025/stream

Title92=Radio la Poderosa 98.2 FM _ Lima _ Perú
File92=http://stream.zeno.fm/8dppgdapxneuv

Title93=Radio La Zona (OCW-4I, 90.5 MHz FM, Lima)
File93=http://playerservices.streamtheworld.com/pls/RADIO_LAZONA.pls

Title94=Radio Level Hits
File94=http://ip.peruhits.com:7234/;

Title95=Radio Los Angeles - Chepen
File95=http://94.23.159.187:9950/;

Title96=Radio Mágica 88.3 FM (OCX-4G, Lima, Perú)
File96=http://playerservices.streamtheworld.com/pls/MAG_AAC.pls

Title97=Radio Marañon 96.1 FM
File97=http://167.114.118.120:7112/;

Title98=Radio Maria Peru (OAX-4M, 580 kHz AM / OBT-4Z 97.7 MHz FM, Lima)
File98=http://dreamsiteradiocp4.com:8020/

Title99=Radio Master
File99=http://ip.perustreaming.net:9740/

Title100=Radio Master
File100=http://ip.perustreaming.net:9740/

Title101=Radio Matucana (102.9 FM, Lima)
File101=http://radio.transmite.pe:9316/;

Title102=RADIO MAXIMA  FM
File102=http://stream.zeno.fm/yxwg54nyg3quv

Title103=RADIO MAXIMA DEL PERU
File103=http://stream.zeno.fm/yxwg54nyg3quv.m3u

Title104=RADIO MAXIMA online
File104=http://stream.zeno.fm/yxwg54nyg3quv

Title105=RADIO MAXIMA PERU
File105=http://stream.zeno.fm/yxwg54nyg3quv.pls

Title106=RADIO MEGATRON MIX
File106=http://stream.zeno.fm/rw67aty148quv.m3u

Title107=RADIO MEGATRON MIX  FM
File107=http://stream.zeno.fm/rw67aty148quv.pls

Title108=Radio Melodía Arequipa (104.3 FM)
File108=http://184.154.28.210:8214/;

Title109=RADIO MIX 103.5
File109=http://stream.zeno.fm/ug8papw4nbruv

Title110=Radio Nova - Chimbote 104.3
File110=http://67.212.179.138:7162/;

Title111=Radio Nova - Piura 94.5
File111=http://67.212.179.138:8054/;

Title112=Radio Nova - Trujillo 105.1
File112=http://67.212.179.138:7064/;

Title113=Radio Nueva Q (OCZ-4P, 107.1 MHz, Lima)
File113=http://playerservices.streamtheworld.com/pls/CRP_NQ.pls

Title114=Radio Oasis (OCX-4U, 100.1 MHz, Lima)
File114=http://playerservices.streamtheworld.com/pls/CRP_OAS.pls

Title115=Radio Ovación
File115=https://5c3fb01839654.streamlock.net:554/ipradioovacion1/liveovacion1radio/chunklist_w2018270091.m3u8

Title116=Radio Oxígeno Lima (OBT-4S, 102.1 MHz)
File116=http://playerservices.streamtheworld.com/pls/RADIO_OXIGENO.pls

Title117=RADIO PERFECCION FM
File117=http://stream.zeno.fm/n350ttc3u8quv.pls

Title118=Radio Pirata Mix - Cajabamba
File118=http://167.114.118.120:7628/;

Title119=Radio Planeta (OCZ-4L, 107.7 MHz FM, Lima)
File119=http://playerservices.streamtheworld.com/pls/CRP_PLA.pls

Title120=Radio Retro Perú
File120=http://listen.radionomy.com/radio-retro-rock--pop.m3u

Title121=Radio Ritmo Romántica
File121=http://playerservices.streamtheworld.com/pls/CRP_RIT.pls

Title122=Radio Rock and Pop - Lima
File122=http://listen.radionomy.com/radiorockandpop.m3u

Title123=Radio Románticas Inolvidables
File123=http://ample-zeno-07.radiojar.com/8tx9pb0y31wtv

Title124=Radio Santa Beatriz 90.5 FM
File124=http://167.114.118.120:7288/;

Title125=Radio Santa Rosa
File125=https://securestream.conectarhosting.com:8136/live

Title126=Radio Telestereo
File126=http://67.212.179.138:8300/;

Title127=Radio Tropicana Paita
File127=http://167.114.118.120:6010/listen.pls?sid=1

Title128=Radio Tushurami Folk
File128=http://5.135.183.124:8431/stream

Title129=Radio Unión
File129=http://67.212.179.138:7218/;

Title130=Radio Uno (Tacna)
File130=https://streamingv2.shoutcast.com/radiounotacna

Title131=Radio Vinilo Perú - Lima
File131=http://167.114.118.120:7246/;

Title132=Radio Viva FM - Lima
File132=http://streamingargentino4.com.ar:5310/;stream.mp3

Title133=Radio Z Rock & Pop
File133=http://167.114.118.120:7440/;

Title134=Radio Z Rock & Pop
File134=http://167.114.118.120:7440/

Title135=RADIO ZOOM FM
File135=http://stream.zeno.fm/bccsy18t25quv.m3u

Title136=Radiomar
File136=http://18323.live.streamtheworld.com/CRP_MAR.mp3?dist=20004

Title137=Radiomar Plus 106.3 Lima
File137=http://playerservices.streamtheworld.com/m3u/CRP_MARAAC.m3u

Title138=RadioSaurios New Age
File138=http://5.135.183.124:8430/stream

Title139=Romántica
File139=http://19473.live.streamtheworld.com/CRP_RIT.mp3

Title140=Romantica Inolvidables
File140=https://node-29.zeno.fm/6m1ba7yt3vduv?rj-ttl=5&rj-tok=AAABcNBKY3IAlJRLSlBFJSTeRg

Title141=Romanticas Inolvidables
File141=https://stream.zeno.fm/6m1ba7yt3vduv

Title142=RPP Noticias
File142=http://playerservices.streamtheworld.com/pls/RADIO_RPP.pls

Title143=RPP Noticias
File143=http://20323.live.streamtheworld.com/RADIO_RPPAAC.aac?tdtok=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImtpZCI6ImZTeXA4In0.eyJpc3MiOiJ0aXNydiIsInN1YiI6IjczMzU0MzM2IiwiaWF0IjoxNTYxMDEwMjI4LCJ0ZC1yZWciOmZhbHNlfQ.T2EHy5YM9Vq2_iFrx2S56UG5c7mSTFot-Y5UAh7YFro

Title144=Sabor
File144=http://node-35.zeno.fm/812ksk66cfeuv.aac?rj-ttl=5&rj-tok=AAABdAovOBcAvbQfInCXPqYUuw

Title145=Sad Perú
File145=http://node-13.zeno.fm/2kma2c011uquv?rj-ttl=5&rj-tok=AAABdApB9sMAVNTCArM6vunMvA

Title146=SAMANTA.... LA SEÑAL JUVENIL
File146=http://listen.radionomy.com/samantalasenaljuvenil.m3u

Title147=San Miguel de Vichaycocha
File147=https://streamingv2.shoutcast.com/radiosanmiguel

Title148=Soberana
File148=http://node-32.zeno.fm/3vw67zg1duquv?rj-ttl=5&rj-tok=AAABdApEtOsAwvxwtuP3IqmO-Q

Title149=Sol Frecuencia Primera - Lima
File149=http://67.212.179.138:8084/;

Title150=Sonrisa-Chachapoyas
File150=https://sonicpanel.globalstream.pro/8028/stream

Title151=Stereo Andina
File151=http://stream.zeno.fm/92et355merruv

Title152=Studio 5
File152=http://stream.zeno.fm/4yfty4xd8yzuv

Title153=studio 5
File153=http://olebu.com:8088/live?type=.mp3

Title154=Studio 92
File154=https://22203.live.streamtheworld.com/RADIO_STUDIO92.mp3

Title155=Studio 92
File155=http://playerservices.streamtheworld.com/pls/RADIO_STUDIO92.pls

Title156=Súper Folk
File156=http://node-14.zeno.fm/baxwy8zv3gruv?rj-ttl=5&rj-tok=AAABdAorjIoAsqeWB6tX-rvprQ

Title157=Súper Vip
File157=https://rautemusik-de-hz-fal-stream18.radiohost.de/charthits

Title158=Telestereo (88 FM, Lima)
File158=http://67.212.179.138:8300/;

Title159=Top Latino
File159=http://online.radiodifusion.net:8020/stream

Title160=Tropical-Tarapoto
File160=https://shoutcast.tmcreativos.com/proxy/radiotropical?mp=/stream

Title161=Xtrema-Chachapoyas
File161=http://node-07.zeno.fm/vv557vfe21zuv.aac?rj-ttl=5&rj-tok=AAABc_pSbowArxi1f7s2NVWq8w

Title162=Xtrema-Chachapoyas
File162=http://node-31.zeno.fm/v2ah096xywzuv.aac?rj-ttl=5&rj-tok=AAABc_pWrSsAoHVwZntGJ1ILxg

Title163=ZOOM  FM
File163=http://stream.zeno.fm/bccsy18t25quv.m3u

