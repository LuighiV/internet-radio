# Internet Radio integration

This project is to explore different capabilities to  reproduce radio stations
in computer software.

## Getting the Internet Radio Configuration
[Radio Station Info](https://www.radio-browser.info/)

This link provides information of radios to get the file for streaming, which
could be used in different programs such as VLC.

This also could be searched by Country in the following link:
[Stations in Peru](https://www.radio-browser.info/#!/bycountry/Peru)

You could download the whole list as .pls format for VLC or M3U for Kodi.

## Reproduce in VLC
To reproduce in VLC just add the corresponding file as a network source.
In this case the file downloaded from the source is a playlist (.pls) file so you could open as a playlist in VLC.

## Reproduce in Kodi
To reproduce in Kodi just load the .m3u playlist format as recommended here
[Add pls to Kodi](https://forum.kodi.tv/showthread.php?tid=306146)

The location folder for playlist in Kodi is [location](https://kodi.wiki/view/Basic_playlists)
which in Linux is ~/.kodi/userdata/playlists/music

So you could add the corresponding file in that directory:
```bash
cp stations/playlist.m3u  ~/.kodi/userdata/playlists/music/
```
## References
This radio stations are available thanks to the project of [Segler Alex](https://github.com/segler-alex)
He has many interesting projects which uses this database as follows:
- [Api Rust](https://github.com/segler-alex/radiobrowser-api-rust)
- [RadioDroid](https://github.com/segler-alex/RadioDroid)
- [Frontend](https://github.com/segler-alex/radiobrowser)
- [Plugin for Kodi](https://github.com/segler-alex/kodi-radio-browser)
